@ECHO OFF

TITLE CaptureChaturbate

pushd "%~dp0"
set savedir=%CD%\captured
mkdir %savedir%

set timeout=%SystemRoot%\system32\timeout.exe

rem ----------------------------------------------------------------------------

mkdir targets

rem start main.bat every 60 seconds for each model
for /r "targets" %%x in (*.txt) do (
start /w /b call maincapture.bat %savedir% %%~nx &&  %timeout% /t 60
)
rem ----------------------------------------------------------------------------

rem mkdir targets2

rem for /r "targets2" %%x in (*.txt) do (
rem start /w /b call maincapture.bat %savedir% %%~nx && %timeout% /t 60
rem )
rem ----------------------------------------------------------------------------

exit